

A few PHP lines to find all answers to a simple geometry problem.

![](https://myally.co/sites/demo/triangle.jpg)

The effect of several different performance improvemements is measured.

[working sample](https://myally.co/sites/demo/triangle.php)