

<img src="https://myally.co/sites/demo/triangle.jpg"/>

<p>produce all possible answers to this simple geometry problem</p>
by inspection: a+b+c = 180 = d+a<br><br>
a &nbsp;&nbsp;b &nbsp;&nbsp;c &nbsp;&nbsp;d<br>

<?php

/* answer digits from $remaining removing digits from $in
if there is a duplicate digit, answer false */
function remainingNumbers($in, $remaining ) {

	foreach ($in as $e) {
		if ($e>99) { //normalize to two digits
			if (!isset($remaining[1])) return false;
			unset($remaining[1]); 
			$e -= 100;
		} 

		if (!isset($remaining[$t = $e % 10])) return false; //ones
		unset($remaining[$t]);

		if (!isset($remaining[$t = floor($e/ 10)])) return false; //tens
		unset($remaining[$t]);
		
	}
	return $remaining;	
}

$answers = [];

$d = 12; //smallest possible angle without reusing digits 012
$numAnswers = 0;
$rustart = getrusage();  

while ($d<179) {
	$a = 180 - $d;
	$rem = range(0,9);  if ($d<100) unset($rem[0]); // leading zeros are a special case
	if ($left = remainingNumbers([$d, $a], $rem)) { //try every permutation of remaing digits for angles b and c; do not reuse digits
		foreach($left as $w) {
			$leftw = $left; unset($leftw[$w]);  //cannot use the same digit again
			foreach($leftw as $x) {
				$leftwx = $leftw; unset($leftwx[$x]);
				foreach($leftwx as $y) {
					$leftwxy = $leftwx; unset($leftwxy[$y]);
					foreach($leftwxy as $z) {
						$b = 10* $x + $w;
						$c = 10*$z + $y;
						if (($b + $c == $d) ) {
							$numAnswers++;
							$answers[]= ($a > 10 ? $a : "0$a") . ' '. ($b > 10 ? $b : "0$b") .' '. ($c > 10 ? $c : "0$c") . ' '. ($d > 99 ? $d : "0$d") . "<br>";
						}
					}
				}
			}
		}
	}
	$d++;
}

foreach ($answers as $e) echo $e;

echo "<br>total answers $numAnswers  found in " . rutime(getrusage(), $rustart, "utime") . " microseconds <br>";


$d = 12; 
$rustart = getrusage();
$numAnswers = 0;
while ($d<179) {
	$a = 180 - $d;
	$rem = range(0,9);  if ($d<100) unset($rem[0]); // leading zeros are a special case
	if ($left = remainingNumbers([$d, $a], $rem)) { //try every possible permutation for b and c; reuse digits and eliminate duplicates
		foreach($left as $w) {
			foreach($left as $x) {
				foreach($left as $y) {
					foreach($left as $z) {
						$b = 10* $x + $w;
						$c = 10*$z + $y;
						if (($b + $c == $d) && remainingNumbers([$b, $c], $left) !== false ) {
							$numAnswers++;
						}
					}
				}
			}
		}
	}
	$d++;
}
echo "<br>filter duplicates<br>total answers $numAnswers  found in " . rutime(getrusage(), $rustart, "utime") . " microseconds <br>";




$d = 12; 
$rustart = getrusage();
$numAnswers = 0;
while ($d<179) {
	$a = 180 - $d;
	$rem = range(0,9);  if ($d<100) unset($rem[0]); // leading zeros are a special case
	if ($left = remainingNumbers([$d, $a], $rem)) { //try every possible permutation for b and c; reuse digits and eliminate duplicates
		foreach($left as $w) {
			foreach($left as $x) {
				foreach($left as $y) {
					foreach($left as $z) {
						$b = 10* $x + $w;
						$c = 10*$z + $y;
						if (remainingNumbers([$b, $c], $left) !== false  &&($b + $c == $d) ) { //always use expensive duplicate test
							$numAnswers++;
						}
					}
				}
			}
		}
	}
	$d++;
}
echo "<br>duplicates with expensive test first<br>total answers $numAnswers  found in " . rutime(getrusage(), $rustart, "utime") . " microseconds <br>";

function rutime($ru, $rus, $index) {
    return (($ru["ru_$index.tv_sec"] - $rus["ru_$index.tv_sec"]) * 1000 + $ru["ru_$index.tv_usec"] - $rus["ru_$index.tv_usec"]);
}
